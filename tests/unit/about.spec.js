import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import About from '@/views/about.vue'

describe('About', () => {
  it('renders "about" text', () => {
    const wrapper = shallowMount(About)
    expect(wrapper.text()).to.include('about')
  })
})

describe('Homepage', () => {
  it('renders "home" and "about" texts', () => {
    const wrapper = shallowMount(App, {
      // without this kind of precaution, tests will fail because it won't
      // know these 2 custom elements: <router link/> and <router-view/>.
      // see: https://vue-test-utils.vuejs.org/guides/using-with-vue-router.html
      stubs: ['router-link', 'router-view', ],
    })
    expect(wrapper.text()).to.include('Home')
    expect(wrapper.text()).to.include('About')
  })
})
