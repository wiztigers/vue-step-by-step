import store from './index'

export const Storage = Object.freeze({
    'none': 0,
    'store': 1,
    'local': 2,
  }); // this is a JS enum

store.subscribe((mutation, state) => { // eslint-disable-line no-unused-vars
  // only save to localStorage if it's requested
  if (mutation.payload.localStorage) {
    const payload = mutation.payload;
    let key = payload.key;
    // reuse the same namespacing as vuex modules
    // see: https://vuex.vuejs.org/guide/modules.html
    const parts = mutation.type.split('/');
    if ( parts.length > 1 ) key = `${parts[0]}/${key}`;
    localStorage.setItem(key, payload.value);
  }
})
