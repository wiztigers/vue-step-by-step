import Vue from 'vue'

export const User = {
  namespaced: true,
  state: {
    name: "John Doe",
    username: "johndoe",
    password: null,
  },
  getters: {
    name: state => { return state.name; },
    username: state => { return state.username; },
    password: state => { return state.password; },
  },
  mutations: {
    update(state, payload) {
      Vue.set(state, payload.key, payload.value);
    }
  },
  actions: {
    update({ commit }, payload) {
        commit('update', payload);
    }
  }
}
