import Vue from 'vue';
// import from vuetify/lib to take advantage of a-la-carte components
// objective: reduce build size in production.
// see documentation: https://vuetifyjs.com/fr-FR/customization/a-la-carte/
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
  },
});
