import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './store/storage'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),  // https://www.w3schools.com/js/js_arrow_function.asp
}).$mount('#app')
