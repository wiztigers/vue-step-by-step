# Vue, step by step

[![pipeline status](https://gitlab.com/wiztigers/vue-step-by-step/badges/master/pipeline.svg)](https://gitlab.com/wiztigers/vue-step-by-step/pipelines)

## See result

See [result on Gitlab pages](https://wiztigers.gitlab.io/vue-step-by-step/)

## Build it
```
npm install
```

### Compile and hot-reload for development
```
npm run serve
```

### Compile and minify for production
```
npm run build
```

### Run unit tests
```
npm run test:unit
```

### Run end-to-end tests
```
npm run test:e2e
```

### Lint and fix files
```
npm run lint
```

## Customize it
See [Configuration Reference](https://cli.vuejs.org/config/).
